<p align="center">
  <img width="500" src=".gitlab/header.png" alt="Blaugue Backend Logo" />
</p>

## Description

This repository contain a [Nest](https://github.com/nestjs/nest) project, that serve an api for [Blaugue](https://blaugue.camponovo.xyz).

## Developers

### Configure
.env
```dotenv
API_URL=http://localhost:3000 # Exposed url
API_PORT=3000 # Api port
MONGO_URI=mongodb://mybase # Valid mongodb connection url

```

### Running the app

- **Install dependencies**
```bash
$ npm install
```
- **Start using npm (with optional flags :dev, :prod)**
```bash
$ npm run start[:dev|:prod]
```

### Dockerized app

- **Build**
```shell
$ docker build -t blaugue-backend .
```

- **Run & specify opened port**
```shell
$ docker run -p 3000:3000 blaugue-backend
```

### Unit tests

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### Deployment

1. Install [p2p](https://github.com/camarm-dev/p2p).
2. Add blaugue beta server with `p2p servers add`
3. Execute deployment with `p2p --exec`

## Acknowledgements

- Code under [Cecill V2.1](LICENSE), by [Silvère <silvere@comtelabs.fr>](https://comtelabs.fr) & [Armand <armand@camponovo.xyz>](https://www.camarm.dev) CAMPONOVO

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

