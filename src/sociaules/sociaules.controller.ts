import {
    Body,
    Controller,
    Delete,
    forwardRef,
    Inject,
    NotFoundException,
    Param,
    Patch,
    Post,
    UnauthorizedException,
    UseInterceptors,
} from '@nestjs/common';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { REQUEST } from '@nestjs/core';
import { SociaulesService } from './sociaules.service';
import { PaustesService } from '../paustes/paustes.service';
import { CaummentesService } from '../caummentes/caummentes.service';
import { Roles } from '../auth/authorization/roles.decorator';
import { BlauguerRole } from '../blauguers/schemas/blauguer-role.enum';
import { Pauste } from '../paustes/schemas/pauste.schema';
import { Caummente } from '../caummentes/schemas/caummente.schema';
import { CreateSociauleForPausteDto } from './dto/create-sociaule-for-pauste-dto';
import { CreateSociauleForCaummenteDto } from './dto/create-sociaule-for-caummente-dto';
import { UpdateSociauleDto } from './dto/update-sociaule-dto';
import { Sociaule } from './schemas/sociaule.schema';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

@UseInterceptors(new SanitizeMongooseModelInterceptor())
@ApiTags('sociaule')
@Controller('sociaules')
export class SociaulesController {
    constructor(
        private SociaulesService: SociaulesService,
        @Inject(forwardRef(() => CaummentesService))
        private CaummentesService: CaummentesService,
        @Inject(forwardRef(() => PaustesService))
        private PaustesService: PaustesService,
        @Inject(REQUEST) private request,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    @Post('/pauste')
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Add a sociaule to a pauste' })
    async createForPauste(
        @Body() createSociauleForPausteDto: CreateSociauleForPausteDto,
    ) {
        const pauste: Pauste = await this.PaustesService.findOne(
            createSociauleForPausteDto.pausteId,
        );
        if (pauste != null) {
            const sociaule: Sociaule = await this.SociaulesService.create(
                createSociauleForPausteDto,
            );
            await this.PaustesService.addSociauleToPauste(
                createSociauleForPausteDto.pausteId,
                sociaule._id.toString(),
            );
            await this.cleanLongTimeCachedResponses();
            return sociaule;
        } else {
            throw new NotFoundException("Pauste's id given not found");
        }
    }

    @Post('/caummente')
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Add a sociaule to a caummente' })
    async createForCaummente(
        @Body() createSociauleForCaummenteDto: CreateSociauleForCaummenteDto,
    ) {
        const caummente: Caummente = await this.CaummentesService.findOne(
            createSociauleForCaummenteDto.caummenteId,
        );
        if (caummente != null) {
            const sociaule: Sociaule = await this.SociaulesService.create(
                createSociauleForCaummenteDto,
            );
            await this.CaummentesService.addSociauleToCaummente(
                createSociauleForCaummenteDto.caummenteId,
                sociaule._id.toString(),
            );
            await this.cleanLongTimeCachedResponses();
            return sociaule;
        } else {
            throw new NotFoundException("Caummente's id given not found");
        }
    }

    @Patch(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: 'Update a caummente, reserved to owner only.',
    })
    async update(
        @Param('id') id: string,
        @Body() updateSociauleDto: UpdateSociauleDto,
    ) {
        const { userId, userRoles, sociaule } = await this.getConstantsFromId(
            id,
        );

        if (
            sociaule.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            await this.cleanLongTimeCachedResponses();
            return this.SociaulesService.update(id, updateSociauleDto);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Delete(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary:
            'Remove a caummente by its id, reserved to owner or administrauteurs.',
    })
    async remove(@Param('id') id: string) {
        const { userId, userRoles, sociaule } = await this.getConstantsFromId(
            id,
        );

        if (
            sociaule.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            await this.PaustesService.cleanPausteFromSociaule(id);
            await this.CaummentesService.cleanCaummenteFromSociaule(id);
            await this.cleanLongTimeCachedResponses();
            return this.SociaulesService.remove(id);
        } else {
            throw new UnauthorizedException();
        }
    }

    private async getConstantsFromId(id: string) {
        const userId: string = this.request.user
            ? this.request.user.blauguerId
            : null;
        const userRoles: BlauguerRole[] = this.request.user
            ? this.request.user.roles
            : null;
        let sociaule: Sociaule = null;
        await this.SociaulesService.findOne(id)
            .then((data) => {
                sociaule = data;
            })
            .catch(() => {
                return null;
            });

        if (sociaule != null) {
            return { userId, userRoles, sociaule };
        } else {
            throw new NotFoundException();
        }
    }

    private async cleanLongTimeCachedResponses() {
        await this.cacheManager.del('/paustes/last30Interauctions');
    }
}

// get all for a pauste
// get all for a comment
