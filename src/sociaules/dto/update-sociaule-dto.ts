import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Emauticone } from '../schemas/emauticone.enum';

export class UpdateSociauleDto {
    @ApiProperty({
        type: String,
        description: 'an emauticone for this Caummente !',
    })
    @IsNotEmpty()
    emauticone: Emauticone;
}
