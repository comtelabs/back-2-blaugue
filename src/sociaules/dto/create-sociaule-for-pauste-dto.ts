import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Pauste } from '../../paustes/schemas/pauste.schema';
import { Emauticone } from '../schemas/emauticone.enum';
import { CreateSociauleDto } from './create-sociaule-dto';

export class CreateSociauleForPausteDto extends CreateSociauleDto {
    @ApiProperty({
        type: Pauste,
        description: "the Pauste's id this Sociaule refers to",
    })
    @IsNotEmpty()
    @IsMongoId()
    pausteId: string;

    @ApiProperty({
        type: String,
        description: 'an emauticone for this Caummente !',
    })
    @IsNotEmpty()
    emauticone: Emauticone;

    @ApiProperty({
        type: Blauguer,
        description: 'the Blaugueur id itself.',
    })
    @IsNotEmpty()
    @IsMongoId()
    author: string;
}
