import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Emauticone } from '../schemas/emauticone.enum';
import { Caummente } from '../../caummentes/schemas/caummente.schema';
import { CreateSociauleDto } from './create-sociaule-dto';

export class CreateSociauleForCaummenteDto extends CreateSociauleDto {
    @ApiProperty({
        type: Caummente,
        description: "the Caummente's id this Sociaule refers to",
    })
    @IsNotEmpty()
    @IsMongoId()
    caummenteId: string;

    @ApiProperty({
        type: String,
        description: 'an emauticone for this Caummente !',
    })
    @IsNotEmpty()
    emauticone: Emauticone;

    @ApiProperty({
        type: Blauguer,
        description: 'the Blaugueur id itself.',
    })
    @IsNotEmpty()
    @IsMongoId()
    author: string;
}
