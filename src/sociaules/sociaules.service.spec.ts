import { Test, TestingModule } from '@nestjs/testing';
import { SociaulesService } from './sociaules.service';

describe('SociaulesService', () => {
    let service: SociaulesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [SociaulesService],
        }).compile();

        service = module.get<SociaulesService>(SociaulesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
