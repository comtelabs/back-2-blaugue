export enum Emauticone {
    Like = "J'adaure",
    Unicorn = 'Licaurne',
    ThumbsUp = 'Taupe',
    Crazy = 'Faulie',
}
