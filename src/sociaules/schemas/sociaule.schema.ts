import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Emauticone } from './emauticone.enum';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type SociauleDocument = Sociaule & Document;

@Schema({
    toJSON: {
        getters: true,
    },
    timestamps: true,
})
export class Sociaule {
    @Prop({
        type: SchemaTypes.ObjectId,
        required: true,
        default: new Types.ObjectId(),
    })
    @ExcludeProperty()
    _id: Types.ObjectId;

    @Prop({
        type: SchemaTypes.ObjectId,
    })
    id: Types.ObjectId;

    @Prop({ required: true })
    emauticone: Emauticone;

    @Prop({
        type: SchemaTypes.ObjectId,
        ref: 'Blauguer',
        required: true,
    })
    author: Blauguer;

    constructor(partial: Partial<Sociaule>) {
        Object.assign(this, partial);
    }
}

export const SociauleSchema = SchemaFactory.createForClass(Sociaule).set(
    'toJSON',
    {
        transform: (doc, ret, options) => {
            ret.id = ret._id;
        },
    },
);
