import { forwardRef, Module } from '@nestjs/common';
import { SociaulesService } from './sociaules.service';
import { SociaulesController } from './sociaules.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Sociaule, SociauleSchema } from './schemas/sociaule.schema';
import { PaustesModule } from '../paustes/paustes.module';
import { CaummentesModule } from '../caummentes/caummentes.module';
import { CacheModule } from '@nestjs/cache-manager';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Sociaule.name, schema: SociauleSchema },
        ]),
        CacheModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                ttl: +configService.get('CACHE_TTL'),
                max: +configService.get('CACHE_MAX_ITEMS'),
                isGlobal: true,
            }),
            inject: [ConfigService],
        }),
        forwardRef(() => PaustesModule),
        forwardRef(() => CaummentesModule),
    ],
    providers: [SociaulesService],
    controllers: [SociaulesController],
    exports: [SociaulesService],
})
export class SociaulesModule {}
