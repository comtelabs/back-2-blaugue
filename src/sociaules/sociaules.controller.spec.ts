import { Test, TestingModule } from '@nestjs/testing';
import { SociaulesController } from './sociaules.controller';

describe('SociaulesController', () => {
    let controller: SociaulesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [SociaulesController],
        }).compile();

        controller = module.get<SociaulesController>(SociaulesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
