import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Sociaule, SociauleDocument } from './schemas/sociaule.schema';
import { UpdateSociauleDto } from './dto/update-sociaule-dto';
import { CreateSociauleDto } from './dto/create-sociaule-dto';

@Injectable()
export class SociaulesService {
    constructor(
        @InjectModel(Sociaule.name)
        private SociauleModel: Model<SociauleDocument>,
    ) {}

    async create(createSociauleDto: CreateSociauleDto): Promise<Sociaule> {
        const createdSociaule = new this.SociauleModel({
            _id: new Types.ObjectId(),
            ...createSociauleDto,
        });
        return await createdSociaule.save();
    }

    async findOne(id: string): Promise<Sociaule> {
        return await this.SociauleModel.findById(id)
            .populate({
                path: 'author',
                select: 'id nickname avatar roles authorized is_banned',
            })
            .exec();
    }

    async update(
        id: string,
        updateSociauleDto: UpdateSociauleDto,
    ): Promise<Sociaule> {
        return await this.SociauleModel.findByIdAndUpdate(
            id,
            updateSociauleDto,
            { new: true },
        )
            .populate({
                path: 'author',
                select: 'id nickname avatar roles authorized is_banned',
            })
            .exec();
    }

    async remove(id: string): Promise<Sociaule> {
        return await this.SociauleModel.findByIdAndDelete(id).exec();
    }
}
