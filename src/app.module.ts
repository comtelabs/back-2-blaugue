import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BlauguersModule } from './blauguers/blauguers.module';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtAuthGuard } from './auth/authentication/jwt-auth.guard';
import { RolesGuard } from './auth/authorization/roles.guard';
import { PaustesModule } from './paustes/paustes.module';
import { CaummentesModule } from './caummentes/caummentes.module';
import { ImaugesModule } from './imauges/imauges.module';
import { SociaulesModule } from './sociaules/sociaules.module';
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }),
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.get<string>('MONGO_URI'),
            }),
            inject: [ConfigService],
        }),
        CacheModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                ttl: +configService.get('CACHE_TTL'),
                max: +configService.get('CACHE_MAX_ITEMS'),
                isGlobal: true,
            }),
            inject: [ConfigService],
        }),
        BlauguersModule,
        AuthModule,
        PaustesModule,
        CaummentesModule,
        ImaugesModule,
        SociaulesModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        { provide: APP_GUARD, useClass: JwtAuthGuard },
        { provide: APP_GUARD, useClass: RolesGuard },
        { provide: APP_INTERCEPTOR, useClass: CacheInterceptor },
    ],
})
export class AppModule {}
