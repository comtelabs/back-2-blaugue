import { Prop } from '@nestjs/mongoose';
import { Document, Number } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class Location extends Document {
    @ApiProperty({ type: String })
    @Prop()
    city: string;

    @ApiProperty({ type: String })
    @Prop()
    country: string;

    @ApiProperty({
        type: [Number],
        description:
            'coordinates explained in WGS84 as [longitude, latitude] where longitude values are between -180 and 180 and latitude values are between -90 and 90',
    })
    @Prop()
    coordinates: [number, number];
}
