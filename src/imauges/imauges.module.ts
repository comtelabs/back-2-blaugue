import { Module } from '@nestjs/common';
import { ImaugesController } from './imauges.controller';
import { ImaugesService } from './imauges.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Imauge, ImaugeSchema } from './schemas/imauge.schema';
import { BlauguersModule } from '../blauguers/blauguers.module';
import { ConfigModule } from '@nestjs/config';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Imauge.name, schema: ImaugeSchema },
        ]),
        BlauguersModule,
        ConfigModule,
    ],
    providers: [ImaugesService],
    controllers: [ImaugesController],
    exports: [ImaugesService],
})
export class ImaugesModule {}
