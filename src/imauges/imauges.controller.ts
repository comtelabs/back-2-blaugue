import {
    Controller,
    FileTypeValidator,
    Get,
    Inject,
    MaxFileSizeValidator,
    NotFoundException,
    Param,
    ParseFilePipe,
    Post,
    Response,
    UnauthorizedException,
    UploadedFile,
    UseInterceptors,
} from '@nestjs/common';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import {
    ApiBody,
    ApiConsumes,
    ApiOkResponse,
    ApiOperation,
    ApiTags,
} from '@nestjs/swagger';
import { REQUEST } from '@nestjs/core';
import { ImaugesService } from './imauges.service';
import { BlauguersService } from '../blauguers/blauguers.service';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { BlauguerRole } from '../blauguers/schemas/blauguer-role.enum';
import { Roles } from '../auth/authorization/roles.decorator';
import { Blauguer } from '../blauguers/schemas/blauguer.schema';
import { Imauge } from './schemas/imauge.schema';
import { Public } from '../auth/authentication/public.decorator';
import sharp from 'sharp';
import { ImaugeResponseDto } from './dto/imauge-response.dto';
import e, { Response as Res } from 'express';
import { ImaugeOrientation } from './schemas/imauge-orientation.enum';
import exifr from 'exifr';
import { CacheTTL } from '@nestjs/cache-manager';

@UseInterceptors(new SanitizeMongooseModelInterceptor())
@ApiTags('imauge')
@Controller('imauges')
export class ImaugesController {
    constructor(
        private ImaugesService: ImaugesService,
        private BlauguersService: BlauguersService,
        private ConfigService: ConfigService,
        @Inject(REQUEST) private request,
    ) {}

    @Post('upload')
    @Roles(BlauguerRole.User)
    @UseInterceptors(FileInterceptor('image'))
    @ApiOperation({ summary: 'upload an imauge and resize it !' })
    @ApiConsumes('multipart/form-data')
    @ApiBody({
        description:
            'IMPORTANT NOTE !!! Only works with a multipart/form-data http request method composed by one field pointing the file to upload',
        examples: {
            a: {
                value: {
                    image: '/path/to/your/local/file',
                },
            },
        },
    })
    @ApiOkResponse({
        type: ImaugeResponseDto,
    })
    async uploadImauge(
        @UploadedFile(
            new ParseFilePipe({
                validators: [
                    new MaxFileSizeValidator({ maxSize: 20000000 }),
                    new FileTypeValidator({ fileType: '/jpeg|jpg|png|gif/' }),
                ],
            }),
        )
        file: Express.Multer.File,
    ) {
        const author: Blauguer = await this.GetAuthorFromUserId();
        const base64_buffer = await this.getBase64Buffer(file, 2048);
        const base64_120px_buffer = await this.getBase64Buffer(file, 120);
        const base64_360px_buffer = await this.getBase64Buffer(file, 360);
        const base64_1024px_buffer = await this.getBase64Buffer(file, 1024);
        const orientation = await this.getOrientation(base64_120px_buffer);
        const imauge: Imauge = await this.ImaugesService.create(
            base64_buffer.toString('base64'),
            base64_120px_buffer.toString('base64'),
            base64_360px_buffer.toString('base64'),
            base64_1024px_buffer.toString('base64'),
            author,
            orientation,
        );
        const main_url: string =
            this.ConfigService.get('API_URL') + '/imauges/';
        return {
            urls: {
                default: main_url + 'default/' + imauge._id,
                1024: main_url + '1024px/' + imauge._id,
                360: main_url + '360px/' + imauge._id,
                120: main_url + '120px/' + imauge._id,
            },
            id: imauge._id.toString(),
        };
    }

    @Get(':id')
    @Roles(BlauguerRole.User)
    async findOne(@Param('id') id: string) {
        const imauge = await this.getImauge(id);
        if (imauge != null) {
            return imauge;
        } else {
            throw new NotFoundException();
        }
    }

    @Get('/default/:id')
    @CacheTTL(0)
    @Public()
    async getBase64(@Response() res: Res, @Param('id') id: string) {
        const imauge = await this.getImauge(id);
        if (imauge != null) {
            const buffer = await this.rotateIfNeeded(
                imauge.base64,
                imauge.orientation,
            );
            this.prepareResponse(res, buffer);
        } else {
            throw new NotFoundException();
        }
    }

    @Get('/1024px/:id')
    @CacheTTL(0)
    @Public()
    async getBase64_1024px(@Response() res: Res, @Param('id') id: string) {
        const imauge = await this.getImauge(id);
        if (imauge != null) {
            const buffer = await this.rotateIfNeeded(
                imauge.base64_1024px ? imauge.base64_1024px : imauge.base64,
                imauge.orientation,
            );
            this.prepareResponse(res, buffer);
        } else {
            throw new NotFoundException();
        }
    }

    @Get('/360px/:id')
    @CacheTTL(0)
    @Public()
    async getBase64_360px(@Response() res: Res, @Param('id') id: string) {
        const imauge = await this.getImauge(id);
        if (imauge != null) {
            const buffer = await this.rotateIfNeeded(
                imauge.base64_360px,
                imauge.orientation,
            );
            this.prepareResponse(res, buffer);
        } else {
            throw new NotFoundException();
        }
    }

    @Get('/120px/:id')
    @CacheTTL(0)
    @Public()
    async getBase64_120px(@Response() res: Res, @Param('id') id: string) {
        const imauge = await this.getImauge(id);
        if (imauge != null) {
            const buffer = await this.rotateIfNeeded(
                imauge.base64_120px,
                imauge.orientation,
            );
            this.prepareResponse(res, buffer);
        } else {
            throw new NotFoundException();
        }
    }

    private async getBase64Buffer(file: Express.Multer.File, size: number) {
        return await sharp(file.buffer)
            .resize(size, size, {
                fit: 'inside',
                withoutEnlargement: true,
            })
            .withMetadata()
            .png()
            .toBuffer();
    }

    private prepareResponse(res: e.Response<any, Record<string, any>>, buffer) {
        res.writeHead(200, {
            'Content-Type': 'image/png',
            'Content-Length': buffer.length,
        });
        res.end(buffer);
    }

    private async rotateIfNeeded(
        string_buffer: string,
        orientation: ImaugeOrientation,
    ) {
        let object = Buffer.from(string_buffer, 'base64');
        if (
            (orientation == ImaugeOrientation.Portrait ||
                (await this.getExifOrientation(object)) ==
                    ImaugeOrientation.Portrait) &&
            (await this.getOrientation(object)) != ImaugeOrientation.Portrait
        ) {
            object = await sharp(object).rotate(90).png().toBuffer();
        }
        return object;
    }

    private async getImauge(id: string) {
        let imauge: Imauge = null;
        await this.ImaugesService.findOne(id)
            .catch()
            .then((data) => {
                imauge = data;
            });
        return imauge;
    }

    private async getOrientation(buffer: Buffer) {
        const exif = await exifr.parse(buffer);
        const exif_orientation = exif.ImageHeight / exif.ImageWidth;
        let orientation = ImaugeOrientation.Square;
        if (exif_orientation > 1) {
            orientation = ImaugeOrientation.Portrait;
        } else if (exif_orientation < 1) {
            orientation = ImaugeOrientation.Landscape;
        }
        return orientation;
    }

    private async getExifOrientation(buffer: Buffer) {
        const exif = await exifr.parse(buffer);
        let orientation = ImaugeOrientation.Square;
        if (exif.Orientation && exif.Orientation.includes('Rotate')) {
            orientation = ImaugeOrientation.Portrait;
        } else {
            orientation = ImaugeOrientation.Landscape;
        }
        return orientation;
    }

    private async GetAuthorFromUserId() {
        const userId: string = this.request.user
            ? this.request.user.blauguerId
            : null;
        if (userId == null) {
            throw new UnauthorizedException();
        }

        let author: Blauguer = null;
        await this.BlauguersService.findOne(userId)
            .then((data) => {
                author = data;
            })
            .catch(() => {
                return null;
            });

        if (author != null) {
            return author;
        } else {
            throw new NotFoundException("author does'nt exist !");
        }
    }
}

// get all for a pauste
// get one for a pauste
// set one
// delete one
