import { Test, TestingModule } from '@nestjs/testing';
import { ImaugesService } from './imauges.service';

describe('ImaugesService', () => {
    let service: ImaugesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImaugesService],
        }).compile();

        service = module.get<ImaugesService>(ImaugesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
