import { Test, TestingModule } from '@nestjs/testing';
import { ImaugesController } from './imauges.controller';

describe('ImaugesController', () => {
    let controller: ImaugesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImaugesController],
        }).compile();

        controller = module.get<ImaugesController>(ImaugesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
