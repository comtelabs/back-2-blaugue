import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { Location } from '../../locations/schemas/location.schema';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { ImaugeOrientation } from './imauge-orientation.enum';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type ImaugeDocument = Imauge & Document;

@Schema({
    toJSON: {
        getters: true,
    },
    timestamps: true,
})
export class Imauge {
    @Prop({
        type: SchemaTypes.ObjectId,
        required: true,
        default: new Types.ObjectId(),
    })
    @ExcludeProperty()
    _id: Types.ObjectId;

    @Prop({
        type: SchemaTypes.ObjectId,
    })
    id: Types.ObjectId;

    @Prop({ required: true })
    base64: string;

    @Prop()
    base64_120px: string;

    @Prop()
    base64_360px: string;

    @Prop()
    base64_1024px: string;

    @Prop()
    orientation: ImaugeOrientation;

    @Prop({
        type: SchemaTypes.ObjectId,
        ref: 'Blauguer',
        required: true,
    })
    author: Blauguer;

    @Prop({ required: true, default: false })
    censured: boolean;

    @Prop({ type: Location })
    location: Location;

    constructor(partial: Partial<Imauge>) {
        Object.assign(this, partial);
    }
}

export const ImaugeSchema = SchemaFactory.createForClass(Imauge).set('toJSON', {
    transform: (doc, ret, options) => {
        ret.id = ret._id;
    },
});
