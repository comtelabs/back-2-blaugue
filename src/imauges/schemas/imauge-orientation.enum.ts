export enum ImaugeOrientation {
    Portrait = 'Paurtrait',
    Landscape = 'Paysauge',
    Square = 'Caurré',
}
