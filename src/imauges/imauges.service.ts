import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Imauge, ImaugeDocument } from './schemas/imauge.schema';
import { Blauguer } from '../blauguers/schemas/blauguer.schema';
import { ImaugeOrientation } from './schemas/imauge-orientation.enum';

@Injectable()
export class ImaugesService {
    constructor(
        @InjectModel(Imauge.name) private ImaugeModel: Model<ImaugeDocument>,
    ) {}

    async create(
        base64: string,
        base64_120px: string,
        base64_360px: string,
        base64_1024px: string,
        author: Blauguer,
        orientation: ImaugeOrientation,
    ): Promise<Imauge> {
        const createdImauge = new this.ImaugeModel({
            _id: new Types.ObjectId(),
            base64,
            base64_120px,
            base64_360px,
            base64_1024px,
            author,
            orientation,
        });
        return await createdImauge.save();
    }

    async findOne(id: string): Promise<Imauge> {
        return await this.ImaugeModel.findById(id).exec();
    }
}
