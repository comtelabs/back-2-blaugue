import { ApiProperty } from '@nestjs/swagger';

export class ImaugeLinksList {
    @ApiProperty({
        type: String,
        description: '2048px max definition of your imauge !',
    })
    default: string;

    @ApiProperty({
        type: String,
        description: '120px definition for thumbnails',
    })
    120: string;

    @ApiProperty({
        type: String,
        description: '360px definition for cards',
    })
    360: string;

    @ApiProperty({
        type: String,
        description: '1024px definition for cards',
    })
    1024: string;
}

export class ImaugeResponseDto {
    @ApiProperty({
        type: ImaugeLinksList,
        description: 'links to different size available for your imauge.',
    })
    urls: ImaugeLinksList;

    @ApiProperty({
        type: String,
        description: 'simply the id',
    })
    id: string;
}
