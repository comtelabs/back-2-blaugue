import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, QueryWithHelpers, Types } from 'mongoose';
import { Caummente, CaummenteDocument } from './schemas/caummente.schema';
import { CreateCaummenteDto } from './dto/create-caummente-dto';
import { UpdateCaummenteDto } from './dto/update-caummente-dto';
import { UpdateCaummenteByModeratorDto } from './dto/update-caummente-by-moderator-dto';

@Injectable()
export class CaummentesService {
    constructor(
        @InjectModel(Caummente.name)
        private CaummenteModel: Model<CaummenteDocument>,
    ) {}

    async create(createCaummenteDto: CreateCaummenteDto): Promise<Caummente> {
        const createdCaummente = new this.CaummenteModel({
            _id: new Types.ObjectId(),
            ...createCaummenteDto,
        });
        return await createdCaummente.save();
    }

    async findOne(id: string): Promise<Caummente> {
        return this.populate(this.CaummenteModel.findById(id));
    }

    async update(
        id: string,
        updateCaummenteDto: UpdateCaummenteDto,
    ): Promise<Caummente> {
        return this.populate(
            this.CaummenteModel.findByIdAndUpdate(id, updateCaummenteDto, {
                new: true,
            }),
        );
    }

    async updateByModerator(
        id: string,
        updateCaummenteByModeratorDto: UpdateCaummenteByModeratorDto,
    ): Promise<Caummente> {
        return this.populate(
            this.CaummenteModel.findByIdAndUpdate(
                id,
                updateCaummenteByModeratorDto,
                {
                    new: true,
                },
            ),
        );
    }

    async addSociauleToCaummente(
        caummenteId: string,
        sociauleId: string,
    ): Promise<Caummente> {
        return await this.CaummenteModel.findByIdAndUpdate(
            caummenteId,
            {
                $addToSet: { sociaules: sociauleId },
            },
            {
                new: true,
            },
        ).exec();
    }

    async remove(id: string): Promise<Caummente> {
        return await this.CaummenteModel.findByIdAndDelete(id).exec();
    }

    async cleanCaummenteFromSociaule(sociauleId: string): Promise<Caummente> {
        return await this.CaummenteModel.findOneAndUpdate(
            {
                sociaules: sociauleId,
            },
            {
                $pull: { sociaules: sociauleId },
            },
            { new: true },
        ).exec();
    }

    private async populate(
        caummente: QueryWithHelpers<any, CaummenteDocument>,
    ) {
        return await caummente
            .populate([
                {
                    path: 'author',
                    select: 'id nickname avatar roles authorized is_banned',
                },
                {
                    path: 'sociaules',
                    select: 'id emauticone author updatedAt',
                    populate: [
                        {
                            path: 'author',
                            select: 'id nickname avatar roles authorized is_banned',
                        },
                    ],
                },
            ])
            .exec();
    }
}
