import { Test, TestingModule } from '@nestjs/testing';
import { CaummentesController } from './caummentes.controller';

describe('CaummentesController', () => {
    let controller: CaummentesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [CaummentesController],
        }).compile();

        controller = module.get<CaummentesController>(CaummentesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
