import { forwardRef, Module } from '@nestjs/common';
import { CaummentesService } from './caummentes.service';
import { CaummentesController } from './caummentes.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Caummente, CaummenteSchema } from './schemas/caummente.schema';
import { PaustesModule } from '../paustes/paustes.module';
import { SociaulesModule } from '../sociaules/sociaules.module';
import { CacheModule } from '@nestjs/cache-manager';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Caummente.name, schema: CaummenteSchema },
        ]),
        CacheModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                ttl: +configService.get('CACHE_TTL'),
                max: +configService.get('CACHE_MAX_ITEMS'),
                isGlobal: true,
            }),
            inject: [ConfigService],
        }),
        forwardRef(() => PaustesModule),
        forwardRef(() => SociaulesModule),
    ],
    providers: [CaummentesService],
    controllers: [CaummentesController],
    exports: [CaummentesService],
})
export class CaummentesModule {}
