import { Test, TestingModule } from '@nestjs/testing';
import { CaummentesService } from './caummentes.service';

describe('CaummentesService', () => {
    let service: CaummentesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [CaummentesService],
        }).compile();

        service = module.get<CaummentesService>(CaummentesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
