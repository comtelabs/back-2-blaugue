import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { Location } from '../../locations/schemas/location.schema';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Sociaule } from '../../sociaules/schemas/sociaule.schema';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type CaummenteDocument = Caummente & Document;

@Schema({
    toJSON: {
        getters: true,
    },
    timestamps: true,
})
export class Caummente {
    @Prop({
        type: SchemaTypes.ObjectId,
        required: true,
        default: new Types.ObjectId(),
    })
    @ExcludeProperty()
    _id: Types.ObjectId;

    @Prop({
        type: SchemaTypes.ObjectId,
    })
    id: Types.ObjectId;

    @Prop({ required: true })
    content: string;

    @Prop({
        type: SchemaTypes.ObjectId,
        ref: 'Blauguer',
        required: true,
    })
    author: Blauguer;

    @Prop({ required: true, default: false })
    censured: boolean;

    @Prop({ type: Location })
    location: Location;

    @Prop({
        type: [
            {
                type: SchemaTypes.ObjectId,
                ref: 'Sociaule',
            },
        ],
    })
    sociaules: Sociaule[];

    constructor(partial: Partial<Caummente>) {
        Object.assign(this, partial);
    }
}

export const CaummenteSchema = SchemaFactory.createForClass(Caummente).set(
    'toJSON',
    {
        transform: (doc, ret, options) => {
            ret.id = ret._id;
        },
    },
);
