import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { UpdateCaummenteDto } from './update-caummente-dto';

export class UpdateCaummenteByModeratorDto extends PartialType(
    UpdateCaummenteDto,
) {
    @ApiProperty({
        type: Boolean,
        description: 'is your Caummente censured by Mauderators ?',
        default: false,
    })
    @IsBoolean()
    censured: boolean;
}
