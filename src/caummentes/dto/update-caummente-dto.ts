import { Location } from '../../locations/schemas/location.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { CreateCaummenteDto } from './create-caummente-dto';

export class UpdateCaummenteDto extends PartialType(CreateCaummenteDto) {
    @ApiProperty({
        type: String,
        description: 'a very interesting content for your Caummente !',
    })
    @IsNotEmpty()
    @IsString()
    content: string;

    @ApiProperty({ type: Location })
    location: Location;
}
