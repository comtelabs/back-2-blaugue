import { Location } from '../../locations/schemas/location.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Sociaule } from '../../sociaules/schemas/sociaule.schema';
import { Pauste } from '../../paustes/schemas/pauste.schema';

export class CreateCaummenteDto {
    @ApiProperty({
        type: Pauste,
        description: "the Pauste's id this Caummente refers to",
    })
    @IsNotEmpty()
    @IsMongoId()
    pausteId: string;

    @ApiProperty({
        type: String,
        description: 'a very interesting content for your Caummente !',
    })
    @IsNotEmpty()
    @IsString()
    content: string;

    @ApiProperty({
        type: Blauguer,
        description: 'the Blaugueur id itself.',
    })
    @IsNotEmpty()
    @IsMongoId()
    author: string;

    @ApiProperty({ type: Location })
    location: Location;
}
