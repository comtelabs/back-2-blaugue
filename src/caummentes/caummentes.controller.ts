import {
    Body,
    Controller,
    Delete,
    forwardRef,
    Get,
    Inject,
    NotFoundException,
    Param,
    Patch,
    Post,
    UnauthorizedException,
    UseInterceptors,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CaummentesService } from './caummentes.service';
import { Roles } from '../auth/authorization/roles.decorator';
import { BlauguerRole } from '../blauguers/schemas/blauguer-role.enum';
import { CreateCaummenteDto } from './dto/create-caummente-dto';
import { Caummente } from './schemas/caummente.schema';
import { UpdateCaummenteDto } from './dto/update-caummente-dto';
import { PaustesService } from '../paustes/paustes.service';
import { Pauste } from '../paustes/schemas/pauste.schema';
import { Public } from '../auth/authentication/public.decorator';
import { SociaulesService } from '../sociaules/sociaules.service';
import { UpdateCaummenteByModeratorDto } from './dto/update-caummente-by-moderator-dto';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

@UseInterceptors(new SanitizeMongooseModelInterceptor())
@ApiTags('caummente')
@Controller('caummentes')
export class CaummentesController {
    constructor(
        private CaummentesService: CaummentesService,
        @Inject(forwardRef(() => PaustesService))
        private PaustesService: PaustesService,
        @Inject(forwardRef(() => SociaulesService))
        private SociaulesService: SociaulesService,
        @Inject(REQUEST) private request,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    @Get(':id')
    @Public()
    async findOne(@Param('id') id: string) {
        let caummente: Caummente = null;
        await this.CaummentesService.findOne(id)
            .catch()
            .then((data) => {
                caummente = data;
            });

        if (caummente != null) {
            return caummente;
        } else {
            throw new NotFoundException();
        }
    }

    @Post()
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Add a caummente to a pauste' })
    async create(@Body() createCaummenteDto: CreateCaummenteDto) {
        const pauste: Pauste = await this.PaustesService.findOne(
            createCaummenteDto.pausteId,
        );
        if (pauste != null) {
            const caummente: Caummente = await this.CaummentesService.create(
                createCaummenteDto,
            );
            await this.PaustesService.addCaummenteToPauste(
                createCaummenteDto.pausteId,
                caummente._id.toString(),
            );
            await this.cleanLongTimeCachedResponses();
            return caummente;
        } else {
            throw new NotFoundException("Pauste's id given not found");
        }
    }

    @Patch(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: 'Update a caummente, reserved to owner only.',
    })
    async update(
        @Param('id') id: string,
        @Body() updateCaummenteDto: UpdateCaummenteDto,
    ) {
        const { userId, userRoles, caummente } = await this.getConstantsFromId(
            id,
        );

        if (
            caummente.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            await this.cleanLongTimeCachedResponses();
            return this.CaummentesService.update(id, updateCaummenteDto);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Patch('/admin/:id')
    @Roles(BlauguerRole.Moderator)
    @ApiOperation({
        summary: 'Manage censure on a caummente, reserved to mauderators only.',
    })
    async updateByModerator(
        @Param('id') id: string,
        @Body() updateCaummenteByModeratorDto: UpdateCaummenteByModeratorDto,
    ) {
        await this.cleanLongTimeCachedResponses();
        return this.CaummentesService.updateByModerator(
            id,
            updateCaummenteByModeratorDto,
        );
    }

    @Delete(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary:
            'Remove a caummente by its id, reserved to owner or administrauteurs.',
    })
    async remove(@Param('id') id: string) {
        const { userId, userRoles, caummente } = await this.getConstantsFromId(
            id,
        );

        if (
            caummente.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            await this.PaustesService.cleanPausteFromCaummente(id);
            for (const sociaule of caummente.sociaules) {
                await this.SociaulesService.remove(sociaule._id.toString());
            }
            await this.cleanLongTimeCachedResponses();
            return this.CaummentesService.remove(id);
        } else {
            throw new UnauthorizedException();
        }
    }

    private async getConstantsFromId(id: string) {
        const userId: string = this.request.user
            ? this.request.user.blauguerId
            : null;
        const userRoles: BlauguerRole[] = this.request.user
            ? this.request.user.roles
            : null;
        let caummente: Caummente = null;
        await this.CaummentesService.findOne(id)
            .then((data) => {
                caummente = data;
            })
            .catch(() => {
                return null;
            });

        if (caummente != null) {
            return { userId, userRoles, caummente };
        } else {
            throw new NotFoundException();
        }
    }

    private async cleanLongTimeCachedResponses() {
        await this.cacheManager.del('/paustes/last30Interauctions');
    }
}

// get all for a pauste
// get all by nickname (with their pauste ids)
