import {
    Body,
    Controller,
    Delete,
    Get,
    Inject,
    NotFoundException,
    Param,
    Patch,
    Post,
    UnauthorizedException,
    UseInterceptors,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { PaustesService } from './paustes.service';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { BlauguerRole } from '../blauguers/schemas/blauguer-role.enum';
import { Roles } from '../auth/authorization/roles.decorator';
import { Pauste } from './schemas/pauste.schema';
import { Public } from '../auth/authentication/public.decorator';
import { CreatePausteDto } from './dto/create-pauste.dto';
import { UpdatePausteDto } from './dto/update-pauste.dto';
import { BlauguersService } from '../blauguers/blauguers.service';
import { Blauguer } from '../blauguers/schemas/blauguer.schema';
import { CaummentesService } from '../caummentes/caummentes.service';
import { SociaulesService } from '../sociaules/sociaules.service';
import { UpdatePausteByModeratorDto } from './dto/update-pauste-by-moderator.dto';
import { CACHE_MANAGER, CacheKey, CacheTTL } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

@UseInterceptors(new SanitizeMongooseModelInterceptor())
@ApiTags('pauste')
@Controller('paustes')
export class PaustesController {
    constructor(
        private PaustesService: PaustesService,
        private CaummentesService: CaummentesService,
        private SociaulesService: SociaulesService,
        private BlauguersService: BlauguersService,
        @Inject(REQUEST) private request,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    @Get('/last10Public')
    @CacheTTL(0)
    @Public()
    @ApiOperation({ summary: 'List last 10 public paustes' })
    @ApiResponse({
        status: 200,
        description: 'The found Paustes',
        type: [Pauste],
    })
    async findLast10Public() {
        return await this.PaustesService.findLast10Public();
    }

    @Get('/last10')
    @CacheTTL(0)
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: "List last 10 Paustes posted on Blaugue's platform",
    })
    @ApiResponse({
        status: 200,
        description: 'The found Paustes',
        type: [Pauste],
    })
    async findLast10() {
        return await this.PaustesService.findLast10();
    }

    @Get('/last10/name/:nickname')
    @CacheTTL(0)
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: 'List last 10 paustes of a Blaugueur by its nickname',
    })
    @ApiResponse({
        status: 200,
        description: 'The found Paustes',
        type: [Pauste],
    })
    async findLast10ByNickname(@Param('nickname') nickname: string) {
        const author: Blauguer = await this.GetAuthorFromNickname(nickname);
        return this.PaustesService.findLast10ByAuthor(author);
    }

    @Get('/name/:nickname')
    @CacheTTL(0)
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: 'List all paustes of a Blaugueur by its nickname',
    })
    @ApiResponse({
        status: 200,
        description: 'The found Paustes',
        type: [Pauste],
    })
    async findAllByNickname(@Param('nickname') nickname: string) {
        const author: Blauguer = await this.GetAuthorFromNickname(nickname);
        return this.PaustesService.findByAuthor(author);
    }

    @Get('/last30Interauctions')
    @CacheTTL(0)
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary:
            "List last 30 Paustes caummented or sociauled by someone on Blaugue's platform",
    })
    @ApiResponse({
        status: 200,
        description: 'The found Paustes',
        type: [Pauste],
    })
    async findLast30Interauctions() {
        return await this.PaustesService.findLast30Interauctions();
    }

    @Get(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary:
            'find a pauste from its id, allowed for all paustes if blauguer is connected.',
    })
    @ApiResponse({
        status: 200,
        description: 'The found Pauste',
        type: Pauste,
    })
    async findOne(@Param('id') id: string) {
        let pauste: Pauste = null;
        await this.PaustesService.findOne(id)
            .catch()
            .then((data) => {
                pauste = data;
            });

        if (pauste != null) {
            return pauste;
        } else {
            throw new NotFoundException();
        }
    }

    @Get('/public/:id')
    @Public()
    @ApiOperation({
        summary:
            'find a pauste from its id, publicly allowed but only for public pauste.',
    })
    @ApiResponse({
        status: 200,
        description: 'The found Pauste',
        type: Pauste,
    })
    async findPublicOne(@Param('id') id: string) {
        let pauste: Pauste = null;
        await this.PaustesService.findOne(id)
            .catch()
            .then((data) => {
                pauste = data;
            });
        if (pauste != null && !pauste.private) {
            return pauste;
        } else if (pauste != null && pauste.private) {
            throw new UnauthorizedException();
        } else {
            throw new NotFoundException();
        }
    }

    @Post()
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Create a pauste !' })
    @ApiBody({
        type: CreatePausteDto,
        examples: {
            a: {
                value: {
                    title: 'an amazing Pauste',
                    content: 'something interesting you know but confidential',
                    author: '648f39139851703228c54970',
                    private: true,
                    categauries: ['Infau', 'Faumille'],
                } as CreatePausteDto,
            },
            b: {
                value: {
                    title: 'another amazing Pauste',
                    content: 'something interesting you know for all the world',
                    author: '648f39139851703228c54970',
                    categauries: ['Spaurt'],
                    location: {
                        city: 'Oupans',
                        country: 'France',
                        coordinates: [6.259, 47.15],
                    },
                } as CreatePausteDto,
            },
        },
    })
    async create(@Body() createPausteDto: CreatePausteDto) {
        await this.cleanLongTimeCachedResponses();
        return this.PaustesService.create(createPausteDto);
    }

    @Patch(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: 'Update a pauste, reserved to owner only.',
    })
    @ApiBody({
        type: UpdatePausteDto,
        description:
            'Update one or several values (listed below) of your pauste by its id.',
        examples: {
            a: {
                value: {
                    title: 'a modified amazing Pauste',
                    content:
                        'something interesting you know still confidential',
                    author: '648f39139851703228c54970',
                    private: true,
                    categauries: ['Infau', 'Faumille'],
                    imauges: ['an Imauge id'],
                    location: {
                        city: 'Oupans',
                        country: 'France',
                        coordinates: [6.259, 47.15],
                    },
                } as UpdatePausteDto,
            },
        },
    })
    async update(
        @Param('id') id: string,
        @Body() updatePausteDto: UpdatePausteDto,
    ) {
        const { userId, userRoles, pauste } = await this.getConstantsFromId(id);

        if (
            pauste.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            await this.cleanLongTimeCachedResponses();
            return this.PaustesService.update(id, updatePausteDto);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Patch('/admin/:id')
    @Roles(BlauguerRole.Moderator)
    @ApiOperation({
        summary: 'Manage censure on a pauste, reserved to mauderators only.',
    })
    @ApiBody({
        type: UpdatePausteByModeratorDto,
        description: 'Fix censured boolean to true or false.',
        examples: {
            a: {
                value: {
                    censured: true,
                } as UpdatePausteByModeratorDto,
            },
        },
    })
    async updateByModerator(
        @Param('id') id: string,
        @Body() updatePausteByModeratorDto: UpdatePausteByModeratorDto,
    ) {
        await this.cleanLongTimeCachedResponses();
        return this.PaustesService.updateByModerator(
            id,
            updatePausteByModeratorDto,
        );
    }

    @Delete(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary:
            'Remove a pauste by its id, reserved to owner or administrauteurs.',
    })
    async remove(@Param('id') id: string) {
        const { userId, userRoles, pauste } = await this.getConstantsFromId(id);

        if (
            pauste.author._id.toString() == userId ||
            userRoles.includes(BlauguerRole.Admin)
        ) {
            for (const sociaule of pauste.sociaules) {
                await this.SociaulesService.remove(sociaule._id.toString());
            }
            for (const caummente of pauste.caummentes) {
                for (const sociaule of caummente.sociaules) {
                    await this.SociaulesService.remove(sociaule._id.toString());
                }
                await this.CaummentesService.remove(caummente._id.toString());
            }
            await this.cleanLongTimeCachedResponses();
            return this.PaustesService.remove(id);
        } else {
            throw new UnauthorizedException();
        }
    }

    private async getConstantsFromId(id: string) {
        const userId: string = this.request.user
            ? this.request.user.blauguerId
            : null;
        const userRoles: BlauguerRole[] = this.request.user
            ? this.request.user.roles
            : null;
        let pauste: Pauste = null;
        await this.PaustesService.findOne(id)
            .then((data) => {
                pauste = data;
            })
            .catch(() => {
                return null;
            });

        if (pauste != null) {
            return { userId, userRoles, pauste };
        } else {
            throw new NotFoundException();
        }
    }

    private async GetAuthorFromNickname(nickname: string) {
        let author: Blauguer = null;
        await this.BlauguersService.findOneByNickname(nickname)
            .then((data) => {
                author = data;
            })
            .catch(() => {
                return null;
            });

        if (author != null) {
            return author;
        } else {
            throw new NotFoundException();
        }
    }

    private async cleanLongTimeCachedResponses() {
        await this.cacheManager.del('/paustes/last10Public');
        await this.cacheManager.del('/paustes/last10');
        const keys = await this.cacheManager.store.keys();
        const nickname_keys = keys.filter((key) => key.indexOf('/name/') > -1);
        await nickname_keys.forEach((key) => {
            this.cacheManager.del(key);
        });
    }
}
