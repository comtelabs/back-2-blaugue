import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, QueryWithHelpers, Types } from 'mongoose';
import { Pauste, PausteDocument } from './schemas/pauste.schema';
import { ConfigService } from '@nestjs/config';
import { CreatePausteDto } from './dto/create-pauste.dto';
import { UpdatePausteDto } from './dto/update-pauste.dto';
import { Blauguer } from '../blauguers/schemas/blauguer.schema';
import { UpdatePausteByModeratorDto } from './dto/update-pauste-by-moderator.dto';
import { ImaugeResponseDto } from '../imauges/dto/imauge-response.dto';
import { InterauctionDto } from './dto/interauction.dto';

@Injectable()
export class PaustesService {
    constructor(
        @InjectModel(Pauste.name) private PausteModel: Model<PausteDocument>,
        private ConfigService: ConfigService,
    ) {}

    async create(createPausteDto: CreatePausteDto): Promise<Pauste> {
        const createdPauste = new this.PausteModel({
            _id: new Types.ObjectId(),
            ...createPausteDto,
        });
        return await createdPauste.save();
    }

    async findByAuthor(author: Blauguer): Promise<Pauste[]> {
        const pauste_list: Promise<Pauste>[] = [];
        await this.PausteModel.find({ author }).then((result) => {
            result.forEach((pauste) => {
                pauste_list.push(this.findOne(pauste._id.toString()));
            });
        });
        return Promise.all(pauste_list);
    }

    async findLast10(): Promise<Pauste[]> {
        const pauste_list: Promise<Pauste>[] = [];
        await this.PausteModel.find(
            { censured: false },
            {},
            { sort: { createdAt: -1 }, limit: 10 },
        ).then((result) => {
            result.forEach((pauste) => {
                pauste_list.push(this.findOne(pauste._id.toString()));
            });
        });
        return Promise.all(pauste_list);
    }

    async findLast10Public(): Promise<Pauste[]> {
        const pauste_list: Promise<Pauste>[] = [];
        await this.PausteModel.find(
            { private: false, censured: false },
            {},
            { sort: { createdAt: -1 }, limit: 10 },
        ).then((result) => {
            result.forEach((pauste) => {
                pauste_list.push(this.findOne(pauste._id.toString()));
            });
        });
        return Promise.all(pauste_list);
    }

    async findLast10ByAuthor(author: Blauguer): Promise<Pauste[]> {
        const pauste_list: Promise<Pauste>[] = [];
        await this.PausteModel.find(
            { author },
            {},
            { sort: { createdAt: -1 }, limit: 10 },
        ).then((result) => {
            result.forEach((pauste) => {
                pauste_list.push(this.findOne(pauste._id.toString()));
            });
        });
        return Promise.all(pauste_list);
    }

    async findLast30Interauctions(): Promise<InterauctionDto[]> {
        const interauctions_list: Promise<InterauctionDto>[] = [];
        await this.PausteModel.aggregate([
            {
                $lookup: {
                    from: 'caummentes',
                    localField: 'caummentes',
                    foreignField: '_id',
                    as: 'caummentes',
                },
            },
            {
                $unwind: '$caummentes',
            },
            {
                $lookup: {
                    from: 'blauguers',
                    localField: 'caummentes.author',
                    foreignField: '_id',
                    as: 'caummentesAuthor',
                },
            },
            {
                $unwind: '$caummentesAuthor',
            },
            {
                $project: {
                    pausteId: { $toString: '$_id' },
                    pausteTitle: '$title',
                    content: '$caummentes.content',
                    authorsNickname: '$caummentesAuthor.nickname',
                    authorsAvatar: '$caummentesAuthor.avatar',
                    interactionType: 'caummente',
                    interactionCreatedAt: '$caummentes.createdAt',
                },
            },
            { $sort: { interactionCreatedAt: -1 } },
            { $limit: 30 },
        ]).then((result) => {
            result.forEach((interauction) => {
                interauctions_list.push(interauction);
            });
        });
        await this.PausteModel.aggregate([
            {
                $lookup: {
                    from: 'sociaules',
                    localField: 'sociaules',
                    foreignField: '_id',
                    as: 'sociaules',
                },
            },
            {
                $unwind: '$sociaules',
            },
            {
                $lookup: {
                    from: 'blauguers',
                    localField: 'sociaules.author',
                    foreignField: '_id',
                    as: 'sociaulesAuthor',
                },
            },
            {
                $unwind: '$sociaulesAuthor',
            },
            {
                $project: {
                    pausteId: { $toString: '$_id' },
                    pausteTitle: '$title',
                    content: '$sociaules.emauticone',
                    authorsNickname: '$sociaulesAuthor.nickname',
                    authorsAvatar: '$sociaulesAuthor.avatar',
                    interactionType: 'sociaule',
                    interactionCreatedAt: '$sociaules.createdAt',
                },
            },
            { $sort: { interactionCreatedAt: -1 } },
            { $limit: 30 },
        ]).then((result) => {
            result.forEach((interauction) => {
                interauctions_list.push(interauction);
            });
        });
        await this.PausteModel.aggregate([
            {
                $lookup: {
                    from: 'caummentes',
                    localField: 'caummentes',
                    foreignField: '_id',
                    as: 'caummentes',
                },
            },
            {
                $unwind: '$caummentes',
            },
            {
                $lookup: {
                    from: 'sociaules',
                    localField: 'caummentes.sociaules',
                    foreignField: '_id',
                    as: 'caummentes.sociaules',
                },
            },
            {
                $unwind: '$caummentes.sociaules',
            },
            {
                $lookup: {
                    from: 'blauguers',
                    localField: 'caummentes.sociaules.author',
                    foreignField: '_id',
                    as: 'sociaulesAuthor',
                },
            },
            {
                $unwind: '$sociaulesAuthor',
            },
            {
                $project: {
                    pausteId: { $toString: '$_id' },
                    pausteTitle: '$title',
                    content: '$caummentes.sociaules.emauticone',
                    authorsNickname: '$sociaulesAuthor.nickname',
                    authorsAvatar: '$sociaulesAuthor.avatar',
                    interactionType: 'sociauleOfComment',
                    interactionCreatedAt: '$caummentes.sociaules.createdAt',
                },
            },
            { $sort: { interactionCreatedAt: -1 } },
            { $limit: 30 },
        ]).then((result) => {
            result.forEach((interauction) => {
                interauctions_list.push(interauction);
            });
        });
        const last30_interauctions: InterauctionDto[] = await Promise.all(
            interauctions_list,
        );

        return last30_interauctions
            .sort((a, b) => {
                return (
                    b.interactionCreatedAt.getTime() -
                    a.interactionCreatedAt.getTime()
                );
            })
            .slice(0, 30);
    }

    async findOne(id: string): Promise<Pauste> {
        return this.populateOne(this.PausteModel.findById(id));
    }

    async update(
        id: string,
        updatePausteDto: UpdatePausteDto,
    ): Promise<Pauste> {
        return this.populateOne(
            this.PausteModel.findByIdAndUpdate(id, updatePausteDto, {
                new: true,
            }),
        );
    }

    async updateByModerator(
        id: string,
        updatePausteByModeratorDto: UpdatePausteByModeratorDto,
    ): Promise<Pauste> {
        return this.populateOne(
            this.PausteModel.findByIdAndUpdate(id, updatePausteByModeratorDto, {
                new: true,
            }),
        );
    }

    async remove(id: string): Promise<Pauste> {
        return await this.PausteModel.findByIdAndDelete(id).exec();
    }

    async addCaummenteToPauste(
        pausteId: string,
        caummenteId: string,
    ): Promise<Pauste> {
        return await this.PausteModel.findByIdAndUpdate(
            pausteId,
            {
                $addToSet: { caummentes: caummenteId },
            },
            {
                new: true,
            },
        ).exec();
    }

    async addSociauleToPauste(
        pausteId: string,
        sociauleId: string,
    ): Promise<Pauste> {
        return await this.PausteModel.findByIdAndUpdate(
            pausteId,
            {
                $addToSet: { sociaules: sociauleId },
            },
            {
                new: true,
            },
        ).exec();
    }

    async cleanPausteFromSociaule(sociauleId: string): Promise<Pauste> {
        return await this.PausteModel.findOneAndUpdate(
            {
                sociaules: sociauleId,
            },
            {
                $pull: { sociaules: sociauleId },
            },
            { new: true },
        ).exec();
    }

    async cleanPausteFromCaummente(caummenteId: string): Promise<Pauste> {
        return await this.PausteModel.findOneAndUpdate(
            {
                caummentes: caummenteId,
            },
            {
                $pull: { caummentes: caummenteId },
            },
            { new: true },
        ).exec();
    }

    private getImaugeUrls(main_url: string, id: string) {
        return {
            urls: {
                default: main_url + 'default/' + id,
                1024: main_url + '1024px/' + id,
                360: main_url + '360px/' + id,
                120: main_url + '120px/' + id,
            },
            id: id,
        } as ImaugeResponseDto;
    }

    private async populateOne(pauste: QueryWithHelpers<any, PausteDocument>) {
        return await pauste
            .populate([
                {
                    path: 'author',
                    select: 'id nickname avatar roles authorized is_banned',
                },
                {
                    path: 'imauges',
                    transform: (imauges, id) => {
                        const main_url: string =
                            this.ConfigService.get('API_URL') + '/imauges/';
                        return this.getImaugeUrls(main_url, id);
                    },
                },
                {
                    path: 'caummentes',
                    select: 'content author sociaules censured updatedAt',
                    populate: [
                        {
                            path: 'author',
                            select: 'nickname avatar',
                        },
                        {
                            path: 'sociaules',
                            select: 'emauticone author updatedAt',
                            populate: [
                                {
                                    path: 'author',
                                    select: 'nickname avatar',
                                },
                            ],
                        },
                    ],
                },
                {
                    path: 'sociaules',
                    select: 'emauticone author updatedAt',
                    populate: [
                        {
                            path: 'author',
                            select: 'nickname avatar',
                        },
                    ],
                },
            ])
            .exec();
    }
}
