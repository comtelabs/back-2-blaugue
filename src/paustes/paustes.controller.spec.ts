import { Test, TestingModule } from '@nestjs/testing';
import { PaustesController } from './paustes.controller';

describe('PaustesController', () => {
    let controller: PaustesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [PaustesController],
        }).compile();

        controller = module.get<PaustesController>(PaustesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
