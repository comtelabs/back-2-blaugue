export enum PausteCategaurie {
    Commons = 'Infau',
    Adventures = 'Auventure',
    Vacation = 'Vaucance',
    Activity = 'Auctivité',
    Family = 'Faumille',
    Sport = 'Spaurt',
    Anecdote = 'Anecdaute',
    Education = 'Éducaution',
    Technology = 'Technaulogie',
    Food = 'Boustifaille',
}
