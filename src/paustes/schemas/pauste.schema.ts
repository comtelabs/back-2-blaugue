import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Location } from '../../locations/schemas/location.schema';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Imauge } from '../../imauges/schemas/imauge.schema';
import { Sociaule } from '../../sociaules/schemas/sociaule.schema';
import { Caummente } from '../../caummentes/schemas/caummente.schema';
import { PausteCategaurie } from './pauste-categaurie.enum';
import { SchemaTypes, Types } from 'mongoose';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type PausteDocument = Pauste & Document;

@Schema({
    toJSON: {
        getters: true,
    },
    timestamps: true,
})
export class Pauste {
    @Prop({
        type: SchemaTypes.ObjectId,
        required: true,
        default: new Types.ObjectId(),
    })
    @ExcludeProperty()
    _id: Types.ObjectId;

    @Prop({
        type: SchemaTypes.ObjectId,
    })
    id: Types.ObjectId;

    @Prop({ required: true })
    title: string;

    @Prop({ required: true })
    content: string;

    @Prop({
        type: SchemaTypes.ObjectId,
        ref: 'Blauguer',
        required: true,
    })
    author: Blauguer;

    @Prop({ required: true, default: false })
    private: boolean;

    @Prop({ required: true, default: false })
    censured: boolean;

    @Prop({ type: Location })
    location: Location;

    @Prop({ required: true, default: [PausteCategaurie.Commons] })
    categauries: PausteCategaurie[];

    @Prop({
        type: [
            {
                type: SchemaTypes.ObjectId,
                ref: 'Caummente',
            },
        ],
    })
    caummentes: Caummente[];

    @Prop({
        type: [
            {
                type: SchemaTypes.ObjectId,
                ref: 'Sociaule',
            },
        ],
    })
    sociaules: Sociaule[];

    @Prop({
        type: [
            {
                type: SchemaTypes.ObjectId,
                ref: 'Imauge',
            },
        ],
    })
    imauges: Imauge[];

    constructor(partial: Partial<Pauste>) {
        Object.assign(this, partial);
    }
}

export const PausteSchema = SchemaFactory.createForClass(Pauste).set('toJSON', {
    transform: (doc, ret, options) => {
        ret.id = ret._id;
    },
});
