import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';
import { UpdatePausteDto } from './update-pauste.dto';

export class UpdatePausteByModeratorDto extends PartialType(UpdatePausteDto) {
    @ApiProperty({
        type: Boolean,
        description: 'is this Pauste censured by Mauderators ?',
    })
    @IsBoolean()
    censured: boolean;
}
