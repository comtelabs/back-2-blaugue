import { Location } from '../../locations/schemas/location.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { PausteCategaurie } from '../schemas/pauste-categaurie.enum';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';

export class InterauctionDto {
    @ApiProperty({
        type: String,
        description: 'the if of the pauste interaucted',
    })
    @IsNotEmpty()
    pausteId: string;

    @ApiProperty({
        type: String,
        description: 'the title of the pauste interaucted',
    })
    @IsNotEmpty()
    pausteTitle: string;

    @ApiProperty({
        type: String,
        description: 'the content of the interauction',
    })
    @IsNotEmpty()
    content: string;

    @ApiProperty({
        type: String,
        description: 'the author nickname of the interauction.',
    })
    @IsNotEmpty()
    authorNickname: string;

    @ApiProperty({
        type: String,
        description: 'the author avatar url of the interauction.',
    })
    @IsNotEmpty()
    authorAvatar: string;

    @ApiProperty({
        type: String,
        description: 'the interauction type.',
    })
    @IsNotEmpty()
    interactionType: string;

    @ApiProperty({
        type: Date,
        description: 'the interauction last created date.',
    })
    @IsNotEmpty()
    interactionCreatedAt: Date;
}
