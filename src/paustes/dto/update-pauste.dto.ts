import { PartialType } from '@nestjs/mapped-types';
import { CreatePausteDto } from './create-pauste.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { Location } from '../../locations/schemas/location.schema';
import { PausteCategaurie } from '../schemas/pauste-categaurie.enum';
import { Blauguer } from '../../blauguers/schemas/blauguer.schema';
import { Imauge } from '../../imauges/schemas/imauge.schema';

export class UpdatePausteDto extends PartialType(CreatePausteDto) {
    @ApiProperty({
        type: String,
        description: 'a pretty title for an amazing Pauste !',
    })
    @IsNotEmpty()
    @IsString()
    title: string;

    @ApiProperty({
        type: String,
        description: 'a very interesting content for your Pauste !',
    })
    @IsNotEmpty()
    @IsString()
    content: string;

    @ApiProperty({
        type: Blauguer,
        description: 'the Blaugueur id itself.',
    })
    @IsNotEmpty()
    @IsMongoId()
    author: string;

    @ApiProperty({
        type: Boolean,
        description: 'is your Pauste reserved to registered Blaugueurs ?',
    })
    @IsBoolean()
    private: boolean;

    @ApiProperty({ enum: PausteCategaurie })
    categauries: PausteCategaurie[];

    @ApiProperty({ type: [Imauge] })
    imauges: string[]; // TODO really needed ?

    @ApiProperty({ type: Location })
    location: Location;
}
