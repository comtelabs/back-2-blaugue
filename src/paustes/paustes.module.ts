import { forwardRef, Module } from '@nestjs/common';
import { PaustesService } from './paustes.service';
import { PaustesController } from './paustes.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Pauste, PausteSchema } from './schemas/pauste.schema';
import { BlauguersModule } from '../blauguers/blauguers.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CaummentesModule } from '../caummentes/caummentes.module';
import { SociaulesModule } from '../sociaules/sociaules.module';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Pauste.name, schema: PausteSchema },
        ]),
        CacheModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                ttl: +configService.get('CACHE_TTL'),
                max: +configService.get('CACHE_MAX_ITEMS'),
                isGlobal: true,
            }),
            inject: [ConfigService],
        }),
        BlauguersModule,
        forwardRef(() => CaummentesModule),
        forwardRef(() => SociaulesModule),
        ConfigModule,
    ],
    providers: [PaustesService],
    controllers: [PaustesController],
    exports: [PaustesService],
})
export class PaustesModule {}
