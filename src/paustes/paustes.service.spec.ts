import { Test, TestingModule } from '@nestjs/testing';
import { PaustesService } from './paustes.service';

describe('PaustesService', () => {
    let service: PaustesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [PaustesService],
        }).compile();

        service = module.get<PaustesService>(PaustesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
