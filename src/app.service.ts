import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
    getHello(): string {
        return 'Hello Blauguer !\n Did you search something ?\n Please visit swagger at /api';
    }
}
