/* eslint-disable prettier/prettier */
import { Model, Types } from 'mongoose';
import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Blauguer, BlauguerDocument } from './schemas/blauguer.schema';
import { CreateBlauguerDto } from './dto/create-blauguer.dto';
import { UpdateBlauguerDto } from './dto/update-blauguer.dto';
import { UpdateBlauguerPasswordDto } from "./dto/update-blauguer-password.dto";
import { UpdateBlauguerByModeratorDto } from "./dto/update-blauguer-by-moderator.dto";

@Injectable()
export class BlauguersService {
    constructor(
        @InjectModel(Blauguer.name) private BlauguerModel: Model<BlauguerDocument>,
    ) {}

    async create(createBlauguerDto: CreateBlauguerDto): Promise<Blauguer> {
        const createdBlauguer = new this.BlauguerModel({
            _id: new Types.ObjectId(),
            ...createBlauguerDto,
        });
        return await createdBlauguer.save();
    }

    async findAll(): Promise<Blauguer[]> {
        return this.BlauguerModel.find().exec();
    }

    async findOne(id: string): Promise<Blauguer> {
        return await this.BlauguerModel.findById(id).exec();
    }

    async findOneByNickname(nickname: string): Promise<Blauguer> {
        return await this.BlauguerModel.findOne({ nickname }).exec();
    }

    async update(id: string, updateBlauguerDto: UpdateBlauguerDto): Promise<Blauguer> {
        return await this.BlauguerModel.findByIdAndUpdate(id, updateBlauguerDto, { new: true }).exec();
    }

    async updatePassword(id: string, updateBlauguerPasswordDto: UpdateBlauguerPasswordDto): Promise<Blauguer> {
        return await this.BlauguerModel.findByIdAndUpdate(id, updateBlauguerPasswordDto, { new: true }).exec();
    }

    async updateByModerator(id: string, updateBlauguerByModeratorDto: UpdateBlauguerByModeratorDto): Promise<Blauguer> {
        return await this.BlauguerModel.findByIdAndUpdate(id, updateBlauguerByModeratorDto, { new: true }).exec();
    }

    async remove(id: string): Promise<Blauguer> {
        return await this.BlauguerModel.findByIdAndDelete(id).exec();
    }
}
