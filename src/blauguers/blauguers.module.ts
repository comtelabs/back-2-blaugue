import { Module } from '@nestjs/common';
import { BlauguersService } from './blauguers.service';
import { BlauguersController } from './blauguers.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Blauguer, BlauguerSchema } from './schemas/blauguer.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Blauguer.name, schema: BlauguerSchema },
        ]),
    ],
    controllers: [BlauguersController],
    providers: [BlauguersService],
    exports: [BlauguersService],
})
export class BlauguersModule {}
