import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { Location } from '../../locations/schemas/location.schema';
import { BlauguerRole } from './blauguer-role.enum';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type BlauguerDocument = Blauguer & Document;

@Schema({
    toJSON: {
        getters: true,
    },
    timestamps: true,
})
export class Blauguer {
    @Prop({
        type: SchemaTypes.ObjectId,
        required: true,
        default: new Types.ObjectId(),
    })
    @ExcludeProperty()
    _id: Types.ObjectId;

    @Prop({
        type: SchemaTypes.ObjectId,
    })
    id: Types.ObjectId;

    @Prop({ required: true, unique: true })
    nickname: string;

    @Prop({ required: true, unique: true })
    email: string;

    @Prop({ required: true })
    @ExcludeProperty()
    password: string;

    @Prop()
    avatar: string;

    @Prop({ type: Location })
    location: Location;

    @Prop({ required: true, default: [BlauguerRole.User] })
    roles: BlauguerRole[];

    @Prop()
    @ExcludeProperty()
    token: string;

    @Prop({ required: true, default: false })
    authorized: boolean;

    @Prop({ required: true, default: false })
    is_banned: boolean;

    constructor(partial: Partial<Blauguer>) {
        Object.assign(this, partial);
    }
}

export const BlauguerSchema = SchemaFactory.createForClass(Blauguer).set(
    'toJSON',
    {
        transform: (doc, ret, options) => {
            ret.id = ret._id;
        },
    },
);
