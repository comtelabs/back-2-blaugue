import { Test, TestingModule } from '@nestjs/testing';
import { BlauguersService } from './blauguers.service';

describe('BlauguersService', () => {
    let service: BlauguersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [BlauguersService],
        }).compile();

        service = module.get<BlauguersService>(BlauguersService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
