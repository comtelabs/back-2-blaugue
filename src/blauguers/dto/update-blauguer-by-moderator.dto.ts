import { PartialType } from '@nestjs/mapped-types';
import { CreateBlauguerDto } from './create-blauguer.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';
import { BlauguerRole } from '../schemas/blauguer-role.enum';

export class UpdateBlauguerByModeratorDto extends PartialType(
    CreateBlauguerDto,
) {
    @ApiProperty({ enum: BlauguerRole, enumName: 'a valid BlauguerRole' })
    role: BlauguerRole;

    @ApiProperty({
        type: String,
        description: "a blaugue token, and don't ask me what it is !",
    })
    token: string;

    @ApiProperty({
        type: Boolean,
        description:
            'is this Blauguer authorized to consult private content and publish new pauste ?',
    })
    @IsBoolean()
    authorized: boolean;

    @ApiProperty({
        type: Boolean,
        description: 'is this Blauguer banned of the team ?',
    })
    @IsBoolean()
    is_banned: boolean;
}
