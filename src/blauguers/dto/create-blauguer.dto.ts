import { Location } from '../../locations/schemas/location.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength, IsString } from 'class-validator';

export class CreateBlauguerDto {
    @ApiProperty({
        type: String,
        description: 'a pretty name for a tricky Blauguer !',
    })
    @IsNotEmpty()
    @IsString()
    @MaxLength(64)
    nickname: string;

    @ApiProperty({ type: String, description: 'must be a verified email !' })
    @IsEmail()
    email: string;

    @ApiProperty({ type: String, description: 'a strong password' })
    @IsNotEmpty()
    @IsString()
    password: string;

    @ApiProperty({
        type: String,
        description: 'path to the avatar image to load',
    })
    @IsString()
    avatar: string;

    @ApiProperty({ type: Location })
    location: Location;
}
