import { PartialType } from '@nestjs/mapped-types';
import { CreateBlauguerDto } from './create-blauguer.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateBlauguerPasswordDto extends PartialType(CreateBlauguerDto) {
    @ApiProperty({ type: String, description: 'a strong password' })
    @IsNotEmpty()
    @IsString()
    password: string;
}
