import { PartialType } from '@nestjs/mapped-types';
import { CreateBlauguerDto } from './create-blauguer.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { Location } from '../../locations/schemas/location.schema';

export class UpdateBlauguerDto extends PartialType(CreateBlauguerDto) {
    @ApiProperty({
        type: String,
        description: 'a pretty name for a tricky Blauguer !',
    })
    @IsNotEmpty()
    @IsString()
    @MaxLength(64)
    nickname: string;

    @ApiProperty({ type: String, description: 'must be a verified email !' })
    @IsEmail()
    email: string;

    @ApiProperty({
        type: String,
        description: 'path to the avatar image to load',
    })
    @IsString()
    avatar: string;

    @ApiProperty({ type: Location })
    location: Location;
}
