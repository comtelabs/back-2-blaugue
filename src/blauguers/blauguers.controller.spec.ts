import { Test, TestingModule } from '@nestjs/testing';
import { BlauguersController } from './blauguers.controller';
import { BlauguersService } from './blauguers.service';

describe('BlauguersController', () => {
    let controller: BlauguersController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [BlauguersController],
            providers: [BlauguersService],
        }).compile();

        controller = module.get<BlauguersController>(BlauguersController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
