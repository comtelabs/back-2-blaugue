import {
    Body,
    Controller,
    Delete,
    Get,
    Inject,
    Param,
    Patch,
    Post,
    UnauthorizedException,
    UseInterceptors,
} from '@nestjs/common';
import { BlauguersService } from './blauguers.service';
import { CreateBlauguerDto } from './dto/create-blauguer.dto';
import { UpdateBlauguerDto } from './dto/update-blauguer.dto';
import { UpdateBlauguerPasswordDto } from './dto/update-blauguer-password.dto';
import { UpdateBlauguerByModeratorDto } from './dto/update-blauguer-by-moderator.dto';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Blauguer } from './schemas/blauguer.schema';
import { Public } from '../auth/authentication/public.decorator';
import { BlauguerRole } from './schemas/blauguer-role.enum';
import { Roles } from '../auth/authorization/roles.decorator';
import { REQUEST } from '@nestjs/core';

@UseInterceptors(new SanitizeMongooseModelInterceptor())
@ApiTags('blaugue')
@Controller('blauguers')
export class BlauguersController {
    constructor(
        private BlauguersService: BlauguersService,
        @Inject(REQUEST) private request,
    ) {}

    @Get()
    @Roles(BlauguerRole.User) // TODO add to description
    @ApiOperation({ summary: 'List all blauguers' })
    findAll() {
        return this.BlauguersService.findAll();
    }

    @Get(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Find a blauguer from its id.' })
    @ApiResponse({
        status: 200,
        description: 'The found Blauguer',
        type: Blauguer,
    })
    findOne(@Param('id') id: string) {
        return this.BlauguersService.findOne(id).catch();
    }

    @Get('/name/:nickname')
    @Roles(BlauguerRole.User)
    @ApiOperation({ summary: 'Find a blauguer from its nickname.' })
    @ApiResponse({
        status: 200,
        description: 'The found Blauguer',
        type: Blauguer,
    })
    findOneByNickname(@Param('nickname') nickname: string) {
        return this.BlauguersService.findOneByNickname(nickname).catch();
    }

    @Public()
    @Post()
    @ApiOperation({ summary: 'Create a blauguer !' })
    @ApiBody({
        type: CreateBlauguerDto,
        examples: {
            a: {
                value: {
                    nickname: 'blauguerist',
                    email: 'blauguer@blaugueme.com',
                    password: 'blaugue_is_fantastic',
                } as CreateBlauguerDto,
            },
            b: {
                value: {
                    nickname: 'blauguerista',
                    email: 'blauguerista@blaugueme.com',
                    password: 'blaugue_is_fantastic',
                    location: {
                        city: 'Oupans',
                        country: 'France',
                        coordinates: [6.259, 47.15],
                    },
                } as CreateBlauguerDto,
            },
        },
    })
    create(@Body() createBlauguerDto: CreateBlauguerDto) {
        return this.BlauguersService.create(createBlauguerDto);
    }

    @Patch(':id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: "Update a blauguer's praufile, reserved to owner only.",
    })
    @ApiBody({
        type: UpdateBlauguerDto,
        description:
            'Update one or several values of your praufile (listed below).',
        examples: {
            a: {
                value: {
                    nickname: 'blauguerista',
                    email: 'blauguerista@blaugueme.com',
                    avatar: '/path/to/the/avatar/image',
                    location: {
                        city: 'Oupans',
                        country: 'France',
                        coordinates: [6.259, 47.15],
                    },
                } as UpdateBlauguerDto,
            },
        },
    })
    async update(
        @Param('id') id: string,
        @Body() updateBlauguerDto: UpdateBlauguerDto,
    ) {
        const userId = this.request.user ? this.request.user.blauguerId : null;
        if (userId == id) {
            return await this.BlauguersService.update(id, updateBlauguerDto);
        } else {
            throw new UnauthorizedException();
        }
    }

    @Patch('/password/:id')
    @Roles(BlauguerRole.User)
    @ApiOperation({
        summary: "Update a blauguer's password, reserved to owner only.",
    })
    @ApiBody({
        type: UpdateBlauguerPasswordDto,
        description: 'This route allow to update only a blauguer password.',
        examples: {
            a: {
                value: {
                    password: 'my_brand_new_password',
                } as UpdateBlauguerPasswordDto,
            },
        },
    })
    async updatePassword(
        @Param('id') id: string,
        @Body() updateBlauguerPasswordDto: UpdateBlauguerPasswordDto,
    ) {
        const userId = this.request.user ? this.request.user.blauguerId : null;
        if (userId == id) {
            return await this.BlauguersService.updatePassword(
                id,
                updateBlauguerPasswordDto,
            );
        } else {
            throw new UnauthorizedException();
        }
    }

    @Patch('/admin/:id')
    @Roles(BlauguerRole.Moderator)
    @ApiOperation({
        summary: "Update blauguer's statuses, reserved to mauderateurs !",
    })
    @ApiBody({
        type: UpdateBlauguerByModeratorDto,
        description: 'Update one or several statuses attached to a blauguer.',
        examples: {
            a: {
                value: {
                    authorized: true,
                    is_banned: false,
                } as UpdateBlauguerByModeratorDto,
            },
        },
    })
    updateByModerator(
        @Param('id') id: string,
        @Body() updateBlauguerByModeratorDto: UpdateBlauguerByModeratorDto,
    ) {
        return this.BlauguersService.updateByModerator(
            id,
            updateBlauguerByModeratorDto,
        );
    }

    @Delete(':id')
    @Roles(BlauguerRole.Admin)
    @ApiOperation({
        summary: 'Remove a blauguer, reserved to administrauteurs.',
    })
    remove(@Param('id') id: string) {
        return this.BlauguersService.remove(id);
    }
}
