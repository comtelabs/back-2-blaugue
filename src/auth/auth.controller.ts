import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiBody, ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { LocalAuthGuard } from './authentication/local-auth.guard';
import { AuthService } from './auth.service';
import { Public } from './authentication/public.decorator';

class LoginParams {
    nickname: string;
    password: string;
}

@ApiTags('authentication')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}
    @Public()
    @UseGuards(LocalAuthGuard)
    @ApiOperation({
        summary:
            "Login to Blaugue's using your nickname and your password. If you are well recognized, the API will throw an access_token",
    })
    @ApiBody({
        type: LoginParams,
        examples: {
            a: {
                value: {
                    nickname: 'dr. jekyll',
                    password: 'a strong secret',
                } as LoginParams,
            },
        },
    })
    @ApiOkResponse({
        description: '200 status delivered with an access token.',
        schema: {
            type: 'string',
            example: {
                access_token:
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuaWNrbmFtZSI6InRlc3RldXIzIiwic3ViIjoiNjQ2MTI1ZmIwMTFmN2E5NDc2MDgwNjVkIiwiaWF0IjoxNjg0MDg4NTkxLCJleHAiOjE2ODQwODg2NTF9.URoiSte9DObMy9ndohl-dXmMu7fBHoVakc9XBM-EWRQ',
            },
        },
    })
    @Post('/login')
    async login(@Request() req) {
        return this.authService.login(req.user);
    }
}
