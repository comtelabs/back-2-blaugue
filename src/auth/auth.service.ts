import { Injectable } from '@nestjs/common';
import { BlauguersService } from '../blauguers/blauguers.service';
import { JwtService } from '@nestjs/jwt';
import { Blauguer } from '../blauguers/schemas/blauguer.schema';

@Injectable()
export class AuthService {
    constructor(
        private blauguersService: BlauguersService,
        private jwtService: JwtService,
    ) {}

    async validateBlauguer(nickname: string, pass: string) {
        const blauguer = await this.blauguersService.findOneByNickname(
            nickname,
        );
        if (blauguer && blauguer.password == pass) {
            return blauguer;
        }
        return null;
    }

    async login(blauguer: Blauguer) {
        const payload = {
            nickname: blauguer.nickname,
            sub: blauguer._id,
            roles: blauguer.roles,
        };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
