import { SetMetadata } from '@nestjs/common';
import { BlauguerRole } from '../../blauguers/schemas/blauguer-role.enum';

export const ROLES_KEY = 'roles';
export const Roles = (...roles: BlauguerRole[]) =>
    SetMetadata(ROLES_KEY, roles);
