FROM node:19.7.0-alpine

# create destination directory
RUN mkdir -p /usr/src/backend
WORKDIR /usr/src/backend

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /usr/src/backend/

# update npm & install dependencies
RUN npm i -g npm@latest
RUN npm install

# build
RUN npm run build

EXPOSE 3000
CMD [ "npm", "run", "start:prod" ]
